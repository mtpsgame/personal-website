import {
  Timeline,
  TimelineConnector,
  TimelineContent,
  TimelineDot,
  TimelineItem,
  timelineItemClasses,
  TimelineOppositeContent,
  timelineOppositeContentClasses,
  TimelineSeparator,
} from '@mui/lab';
import { Chip, Typography, useMediaQuery, useTheme } from '@mui/material';
import { type ReactNode, useEffect, useState } from 'react';
import ExperienceService from '../../services/experience';
import { type ExperienceItem } from '../../types';

export function Experience(): ReactNode {
  const theme = useTheme();
  const [experiences, setExperiences] = useState<ExperienceItem[]>([]);

  const isMediaLarge = useMediaQuery(theme.breakpoints.up('lg'));
  const isMediaMedium = useMediaQuery(theme.breakpoints.between('md', 'lg'));
  const isMediaSmall = useMediaQuery(theme.breakpoints.between('xs', 'md'));

  const leftAlignTimelineMedium = {
    [`& .${timelineOppositeContentClasses.root}`]: {
      flex: 0.2,
    },
  };
  const leftAlignTimelineSmall = {
    [`& .${timelineItemClasses.root}:before`]: {
      flex: 0,
      padding: 0,
    },
  };

  useEffect(() => {
    setExperiences(ExperienceService.getExperiences());
  }, []);

  return (
    <>
      <Typography variant="h2" align="center">
        Experience
      </Typography>
      <Timeline
        position={!isMediaLarge ? 'right' : 'alternate'}
        sx={() => {
          const alignment = isMediaMedium
            ? leftAlignTimelineMedium
            : isMediaSmall
              ? leftAlignTimelineSmall
              : {};
          return {
            ...alignment,
            padding: '0.5rem',
          };
        }}
      >
        {experiences.map((experience, index) => (
          <TimelineItem key={index}>
            {!isMediaSmall && (
              <TimelineOppositeContent>
                {isMediaMedium && (
                  <>
                    <Typography variant="subtitle2">
                      {experience.date.start} -
                    </Typography>
                    <Typography variant="subtitle2">
                      {experience.date.end}
                    </Typography>
                  </>
                )}
                {isMediaLarge && (
                  <Typography variant="subtitle2" fontStyle="italic">
                    {experience.date.start} - {experience.date.end}
                  </Typography>
                )}
              </TimelineOppositeContent>
            )}

            <TimelineSeparator>
              <TimelineDot color="primary" />
              {experiences.length > index + 1 && (
                <TimelineConnector sx={{ bgcolor: 'primary.dark' }} />
              )}
            </TimelineSeparator>
            <TimelineContent
              sx={{ paddingTop: '0.1rem', paddingBottom: '3rem' }}
            >
              <Typography variant="h5" color="primary">
                {experience.title}
              </Typography>
              {isMediaSmall && (
                <Typography
                  color="primary.dark"
                  fontStyle="italic"
                  variant="subtitle2"
                >
                  {experience.date.start} - {experience.date.end}
                </Typography>
              )}
              <Typography color="primary.dark" variant="subtitle1">
                {experience.company}
              </Typography>

              {experience.descriptionParagraphs.map((para, key) => (
                <Typography key={key} paragraph textAlign="justify">
                  {para}
                </Typography>
              ))}
              {experience.technologies.map((tech, key) => (
                <Chip key={key} variant="outlined" label={tech} />
              ))}
            </TimelineContent>
          </TimelineItem>
        ))}
      </Timeline>
    </>
  );
}
