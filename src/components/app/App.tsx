import { Container } from '@mui/material';
import Experience from '../experience';
import Projects from '../projects';
import Splash from '../splash';
import { type ReactNode } from 'react';

export function App(): ReactNode {
  return (
    <Container maxWidth="xl">
      <Splash />
      <Experience />
      <Projects />
    </Container>
  );
}
