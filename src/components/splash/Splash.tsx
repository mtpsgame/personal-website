import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitSquare, faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { faSquareEnvelope } from '@fortawesome/free-solid-svg-icons';
import { Box, Grid, type SxProps, Typography } from '@mui/material';
import './splash.scss';
import { accentColour, accentFontFamily } from '../../constants/style';
import { type ReactNode, type CSSProperties } from 'react';

const accentTextStyle: CSSProperties = {
  fontFamily: accentFontFamily,
  color: accentColour,
  wordSpacing: '-0.3rem',
};

const lineStyle: SxProps = {
  fontSize: '1.5rem',
  marginBottom: '0.5rem',
};

const lineIconStyle: SxProps = {
  ...lineStyle,
  width: '2rem',
  marginRight: '1rem',
};

export function Splash(): ReactNode {
  return (
    <Grid container alignItems="center" marginBottom="2rem">
      <Grid
        item
        xs={12}
        md={5}
        minHeight="20rem"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Box display="block">
          <Typography variant="h1">Matthew</Typography>
          <Typography variant="h1">Turton</Typography>
          <Typography variant="h1">Parry</Typography>
        </Box>
      </Grid>
      <Grid
        item
        xs={12}
        md={7}
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="flux-start"
      >
        <Box>
          <Grid container display="inline-block">
            <Grid item display="flex" xs={12}>
              <Typography sx={lineIconStyle} paragraph>
                🖥️
              </Typography>
              <Typography sx={lineStyle}>
                Full stack{' '}
                <span style={accentTextStyle}>Software Engineer</span> at{' '}
                <span style={accentTextStyle}>Flipdish</span>
              </Typography>
            </Grid>
            <Grid item display="flex" xs={12}>
              <Typography sx={lineIconStyle} paragraph>
                📍
              </Typography>
              <Typography sx={lineStyle}>
                Based in <span style={accentTextStyle}>Manchester, UK</span>
              </Typography>
            </Grid>
            <Grid item display="flex" xs={12}>
              <Typography sx={lineIconStyle} paragraph>
                🎓
              </Typography>
              <Typography sx={lineStyle}>
                <span style={accentTextStyle}>University of York - MEng</span>,
                graduated 2020
              </Typography>
            </Grid>
          </Grid>
        </Box>
        <Box
          color="primary.dark"
          display="flex"
          gap="3rem"
          justifyContent="center"
          sx={{ marginTop: '2rem' }}
        >
          <a
            className="iconLink"
            href="https://www.linkedin.com/in/matthewturtonparry/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon size="3x" icon={faLinkedin} />
          </a>
          <a
            className="iconLink"
            href="https://gitlab.com/mtpsgame"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon size="3x" icon={faGitSquare} />
          </a>
          <a
            className="iconLink"
            href="mailto:matthewturtonparry@gmail.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon size="3x" icon={faSquareEnvelope} />
          </a>
        </Box>
      </Grid>
    </Grid>
  );
}
