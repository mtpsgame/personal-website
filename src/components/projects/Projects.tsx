import { Masonry } from '@mui/lab';
import { Card, CardContent, Chip, Link, Typography } from '@mui/material';
import { type ReactNode, useEffect, useState } from 'react';
import ProjectService from '../../services/projects';
import { type ProjectItem } from '../../types';

export function Projects(): ReactNode {
  const [projects, setProjects] = useState<ProjectItem[]>([]);

  useEffect(() => {
    setProjects(ProjectService.getProjects());
  }, []);

  return (
    <>
      <Typography variant="h2" align="center">
        Projects
      </Typography>
      <Masonry spacing={2} columns={{ xs: 1, md: 2, xl: 3 }}>
        {projects.map((project, index) => (
          <Card key={index} sx={{ borderRadius: '10px' }}>
            <CardContent>
              <Typography variant="h5" marginBottom="1rem">
                {project.name}
              </Typography>
              {project.descriptionParagraphs.map((para, key) => (
                <Typography key={key} paragraph textAlign="justify">
                  {para}
                </Typography>
              ))}
              <div>
                {project.technologies.map((tech, key) => (
                  <Chip key={key} variant="outlined" label={tech} />
                ))}
              </div>
              <div style={{ marginTop: '1rem' }}>
                {project.links.map((link, key) => (
                  <Link
                    key={key}
                    display="inline-block"
                    href={link.href}
                    underline="hover"
                    target="_blank"
                    rel="noopener"
                    marginRight="1.5rem"
                  >
                    {link.name}
                  </Link>
                ))}
              </div>
            </CardContent>
          </Card>
        ))}
      </Masonry>
    </>
  );
}
