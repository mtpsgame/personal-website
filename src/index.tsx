import {
  createTheme,
  CssBaseline,
  type ThemeOptions,
  ThemeProvider,
} from '@mui/material';
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './components/app';
import { accentColour, accentFontFamily } from './constants/style';
import './index.css';

const themeOptions: ThemeOptions = {
  components: {
    MuiChip: {
      styleOverrides: {
        root: {
          margin: '0.2rem',
          fontFamily: accentFontFamily,
        },
      },
    },
    MuiCard: {
      styleOverrides: {
        root: {
          boxShadow: `0px 0px 7px -2px ${accentColour}`,
        },
      },
    },
  },
  palette: {
    primary: {
      main: accentColour,
    },
    secondary: {
      main: '#000000',
    },
    background: {
      default: '#0a1a2f',
      paper: '#001727',
    },
    text: {
      primary: '#bdbdbd',
    },
  },
  typography: {
    h1: {
      color: accentColour,
      fontSize: '3rem',
      fontWeight: 700,
      fontFamily: accentFontFamily,
      letterSpacing: '0.1em',
      lineHeight: '4rem',
    },
    h2: {
      color: accentColour,
      fontSize: '2.5rem',
      fontFamily: accentFontFamily,
      fontWeight: 700,
      letterSpacing: '0.1em',
      marginBottom: '1rem',
    },
    h3: {
      color: accentColour,
      fontSize: '2rem',
      fontFamily: accentFontFamily,
      fontWeight: 300,
      letterSpacing: '0.05em',
    },
    h4: {
      color: accentColour,
      fontSize: '1.8rem',
      fontFamily: accentFontFamily,
      fontWeight: 300,
      letterSpacing: '0.05em',
    },
    h5: {
      color: accentColour,
      fontSize: '1.5rem',
      fontFamily: accentFontFamily,
      fontWeight: 400,
    },
    h6: {
      color: accentColour,
      fontSize: '1.3rem',
      fontFamily: accentFontFamily,
      fontWeight: 400,
    },
    button: {
      fontFamily: accentFontFamily,
    },
    subtitle1: {
      fontFamily: accentFontFamily,
      letterSpacing: '-0.05em',
      fontWeight: 300,
    },
    subtitle2: {
      fontFamily: accentFontFamily,
      letterSpacing: '-0.05em',
      fontWeight: 300,
    },
  },
};
const theme = createTheme(themeOptions);

const rootElement = document.getElementById('root');
if (rootElement !== null) {
  const root = ReactDOM.createRoot(rootElement);
  root.render(
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </React.StrictMode>
  );
}
