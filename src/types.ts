interface ExperienceItem {
  company: string;
  title: string;
  date: { start: string; end: string };
  descriptionParagraphs: string[];
  technologies: string[];
}

interface ProjectItem {
  name: string;
  descriptionParagraphs: string[];
  technologies: string[];
  links: Array<{ name: string; href: string }>;
}

export type { ExperienceItem, ProjectItem };
