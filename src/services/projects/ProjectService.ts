import { type ProjectItem } from '../../types';

const projects: ProjectItem[] = [
  {
    name: 'Spotify Visualiser Using Philips Hue',
    descriptionParagraphs: [
      'After learning that Spotify had audio analysis data available via their API, I came up with the idea of turning that data into light, with the help of Philips Hue smart bulbs. I created an Angular frontend and a collection of RESTful API microservices that together combine to form a visualiser for the currently playing Spotify song.',
      "Using the Angular SPA, users can login to both their Spotify account and local Philips Hue bridge. After doing so, a list of lights connected to their bridge is displayed, and the user can select which lights they wish to include in the visualisation. Additionally, for each light they can select the visualisation type; what features of a song the light reacts to and what colours to use. After clicking the start button, the selected lights will react to the currently playing song on the user's Spotify account. If the song is paused, then the visualisation also pauses.",
      'The visualisation logic is housed its own microservice, with a data stream (TCP socket) established between it and the frontend upon starting a visualisation session. With this architecture, multiple visualisation sessions by different users in any location will be able to take place.',
    ],
    links: [
      {
        name: 'View on GitLab',
        href: 'https://gitlab.com/mtpsgame/spotify-visualiser-with-philips-hue',
      },
    ],
    technologies: [
      'Python',
      'Flask',
      'Socket.io',
      'Angular',
      'TypeScript',
      'Docker',
      'Docker Compose',
    ],
  },
  {
    name: 'Dissertation: Twitter Sentiment Predicts Stock Price Movements',
    descriptionParagraphs: [
      'Using the Twitter API, I gathered tweets posted over a 12-month period that contained keywords relating to Tesla stock. Each tweet was sent through a natural language sentiment binary classifier (Naive Bayes) to categorise them as either positive or negative. The percentage of tweets that were positive for each day in the 12-month period formed a time series.',
      'I trained two long short-term memory (LSTM) neural networks to predict the net direction of the stock price movement from one day to the next (a binary classification problem; either up or down). The difference between the two neural networks were the inputs they received; one receive the normalised historical price data time series as input and the other received both the historical price data and Twitter sentiment time series as input. The architecture and hyperparameters of both neural networks were varied to examine the effect they had. The networks were trained on 10 months worth of data, and tested on the one remaining month.',
      'The dissertation explains the motivation behind such a project by comparing it to related academic literature. The accuracies of predictions made by each hyperparameter permutation of both neural networks are presented, and comparisons made.',
    ],
    links: [
      {
        name: 'View on GitLab',
        href: 'https://gitlab.com/mtpsgame/twitter-predicts-stock-price-movements',
      },
      {
        name: 'View Dissertation',
        href: 'static/MatthewTurtonParryDissertation.pdf',
      },
    ],
    technologies: [
      'Python',
      'Keras',
      'Scikit-Learn',
      'Natural Language Toolkit',
      'MATLAB',
      'LaTeX',
    ],
  },
  {
    name: 'Binary Trading Strategy Backtesting',
    descriptionParagraphs: [
      'I created a Python library that can be used to backtest a binary trading technical strategy on historical price data provided by the user. Using the library, a set of rules defining when to buy and sell can be created programmatically. A simulation is then run using these rules on the historical price data, after which the total profit / loss is calculated, taking into account any commission payable to the broker on profitable trades.',
      'To define a strategy, the user must select which technical indicators they wish to use (e.g. RSI, MACD, Bollinger Bands etc) and triggers / signals that will cause a buy or sell (e.g. RSI indicates oversold, price has left the Bollinger Band range etc). The parameters used to setup the indicators are fully customisable, and rules of when to buy and sell can be a complex combination of individual indicator signals. Users can implement their own custom indicators and signals should they need to, by implementing base classes.',
    ],
    links: [
      {
        name: 'View on GitLab',
        href: 'https://gitlab.com/mtpsgame/binary-trading-strategy-backtesting',
      },
    ],
    technologies: ['Python', 'NumPy'],
  },
  {
    name: "Master's Group Project: Making York a Smart City. A Data-Driven Dynamic Congestion Charge",
    descriptionParagraphs: [
      "As part of my master's year I worked on a group project in a team of six with the brief of making York a smart city within the next five years, in a way that must benefit local businesses. The idea we developed was to implement a dynamic congestion charge, driven by a range of real-time data sets. Many studies of city centres around the world have shown that dynamic congestion charges tend to increase footfall, which would benefit the heavily tourist and retail based city centre economy.",
      'We developed a prototype system which consisted of a Vue.js frontend and Python backend. The frontend was aimed at the road users of York, and displayed the congestion charge information visually on a map. Users could pre-purchase tickets which were typically cheaper than on-demand prices. The backend process read real-time data sets such as footfall, traffic counts and emissions levels, and used these to set a charge for each area of York. In our report we outlined future work, including the development of a stacked AI model that would predict future congestion charges (by predicting the future values of footfall, traffic counts etc).',
      'I was the designated technical lead on the project and outlined the architecture of the backend. Along with the other backend engineers, I helped write the algorithms that computed the congestion charges. In addition, I took ownership of the API that allowed the frontend and backend to communicate.',
    ],
    links: [
      {
        name: 'View on GitLab',
        href: 'https://gitlab.com/mtpsgame/gpig',
      },
      {
        name: 'View Report',
        href: 'static/MastersProject.pdf',
      },
    ],
    technologies: ['Python', 'Flask', 'Vue.js', 'JvaScript', 'Docker', 'LaTeX'],
  },
  {
    name: 'Genetic Algorithm Simulation',
    descriptionParagraphs: [
      'I implemented and assigned a feedforward neural network (MLP) with random weights to each individual in a population of agents acting in a 2D plane. The outputs of the neural networks control the movement of the agent, one to control the forward speed and the other to determine the change in rotation. The aim of the agents is to navigate an obstacle course without colliding with any objects; the further they get, the larger their fitness. Individuals have five sensors that detect the distance to the closest object in different directions. These sensor readings form the input to the neural networks.',
      "After all individuals in a population collide with an obstacle, genetic selection, crossover and mutation occur. Selection is tournament based, with fitter individuals more likely to be selected. Crossover is done on a per weight basis, with an equal chance for each parent that their corresponding weighting will be chosen. Mutations are also applied on a per weight basis, and randomly change a weighting's value with a given probability.",
    ],
    links: [
      {
        name: 'View on GitLab',
        href: 'https://gitlab.com/mtpsgame/neural-network-evolution',
      },
    ],
    technologies: ['C#', 'Unity'],
  },
];

export const ProjectService = {
  getProjects(): ProjectItem[] {
    return projects;
  },
};
