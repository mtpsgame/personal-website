data "aws_acm_certificate" "domain_cert" {
  provider = aws.aws-cert-provider
  domain   = local.domain_name
  statuses = ["ISSUED"]
}

data "aws_cloudfront_cache_policy" "caching_optimsed" {
  name = "Managed-CachingOptimized"
}
