terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "http" { }
}

provider "aws" {
  region = "eu-west-2"
  default_tags {
    tags = {
      created-by = "terraform"
      application = "personal-website"
    }
  }
}

# ACM certs are deployed in us-east-1
provider "aws" {
  region = "us-east-1"
  alias = "aws-cert-provider"
  default_tags {
    tags = {
      created-by = "terraform"
      application = "aws-governance"
    }
  }
}
